package com.employee.management.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.employee.management.model.EmployeeModel;
import com.employee.management.service.DepartmentService;
import com.employee.management.service.EmployeeService;

@Controller
public class HomeController {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	DepartmentService departmentService;

	@GetMapping(value = { "/employee/create", "/welcome" })
	public ModelAndView index() {
		ModelAndView model = new ModelAndView();
		model.addObject("departments", departmentService.getAllDepertments());
		model.addObject("managers", employeeService.getManagers());
		model.setViewName("employeeadd");
		return model;
	}

	@PostMapping("/employee/save")
	public ModelAndView saveEmployee(@ModelAttribute("employee") EmployeeModel employee) {
		ModelAndView model = new ModelAndView();
		model.addObject("employee", employeeService.createEmployee(employee));
		model.setViewName("employeeview");

		return model;
	}

	
	@GetMapping("/employee/view/{employeeId}")
	public ModelAndView getEmployee(@PathVariable  long employeeId) {
		ModelAndView model = new ModelAndView();
		model.addObject("employee", employeeService.getEmployee(employeeId));
		model.setViewName("employeeview");

		return model;
	}
	
	@GetMapping("employees")
	public ModelAndView listEmployee() {
		ModelAndView model = new ModelAndView();
		model.addObject("employees", employeeService.getEmployee());
		model.setViewName("employees");
		return model;
	}
}