package com.employee.management.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data 
public class DepartmentModel implements Serializable {

	private static final long serialVersionUID = 1L;
 
	private Long departmentId;
 
	private String departmentName; 
}
