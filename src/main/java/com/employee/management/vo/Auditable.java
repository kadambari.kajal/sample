package com.employee.management.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
public class Auditable<U> implements Serializable {

	private static final long serialVersionUID = 1L;

	@CreatedBy
	@Column(name = "CREATED_BY", updatable = false)
	protected U createdBy;

	@LastModifiedBy
	@Column(name = "UPDATED_BY", updatable = true)
	protected U updatedBy;

	@CreatedDate
	@Column(name = "CREATED_TIME", updatable = false)
	protected LocalDateTime createdTime;

	@LastModifiedDate
	@Column(name = "UPDATED_TIME", updatable = true)
	protected LocalDateTime updatedTime;

}
