package com.employee.management.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

//RFC 7807 responses
@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler(UserNotFoundException.class)
	public ModelAndView handleResourceNotFound(final UserNotFoundException exception) {
		ModelAndView model = new ModelAndView();
		model.addObject("error", exception.getMessage());
		model.setViewName("errorpage");

		return model;
	}

	@ExceptionHandler(InternalServerException.class)
	public ModelAndView handleInternalServerException(final InternalServerException exception) {
		ModelAndView model = new ModelAndView();
		model.addObject("error", exception.getMessage());
		model.setViewName("errorpage");

		return model;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleResourceNotFound(final Exception exception) {
		ModelAndView model = new ModelAndView();

		model.addObject("error", "Internal Server Error ");
		model.setViewName("errorpage");

		return model;
	}
}
