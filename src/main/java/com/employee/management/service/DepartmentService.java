package com.employee.management.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.management.model.DepartmentModel;
import com.employee.management.repositories.DepartmentRepository;
import com.employee.management.vo.Department;

@Service
public class DepartmentService {

	@Autowired
	DepartmentRepository departmentRepository;

	public List<DepartmentModel> getAllDepertments() {
		List<Department> findAll = departmentRepository.findAll();
		 return findAll.stream().map(department -> {
			DepartmentModel departmentModel = new DepartmentModel(); 
			departmentModel.setDepartmentId(department.getId());
			departmentModel.setDepartmentName(department.getDepartmentName());
			return departmentModel;
		}).collect(Collectors.toList()); 
	}
	
 
}
